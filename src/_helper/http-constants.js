import axios from 'axios';

let baseURL = process.env.baseURL || "https://djjumoflowapp.com/djjumoflow/api";

let instance = axios.create({
	baseURL
});

export const HTTP = instance;