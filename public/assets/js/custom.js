// carousel 
$(document).ready(function() {

    $("#banner-slider").owlCarousel({
        autoplay: false,
        loop: false,
        margin: 20,
        autoplayTimeout: 7000,
        smartSpeed: 800,
        nav: true,
        dot: true,
        responsive: {
            0: {
                items: 1
            },

            600: {
                items: 1
            },

            1024: {
                items: 1
            },

            1366: {
                items: 1
            }
        }
    });

});
// end scroll


// increment decrement 
    const minus = $('.quantity__minus');
    const plus = $('.quantity__plus');
    const input = $('.quantity__input');
    minus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        if (value > 1) {
            value--;
        }
        input.val(value);
    });

    plus.click(function(e) {
        e.preventDefault();
        var value = input.val();
        value++;
        input.val(value);
    })



//  back to top

var btn = $('#button');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});

// scroll to fix header
$(window).scroll(function() {
    // $(document).on('click', '.mobile-icon', function () {
    if ($(this).scrollTop() > 120) {
        $('.header').addClass('fixed');
    } else {
        $('.header').removeClass('fixed');
    }
});
// smooth scroll to anchor, with option of hash appearing in url. Thanks:
    // $('a[href^="#"]').on('click', function(e) {
    //     e.preventDefault();
    //     var target = this.hash;
    //     var $target = $(target);
    //     $('html, body').stop().animate({
    //         'scrollTop': $target.offset().top - 83
    //     }, 900, 'swing', function() {
    //         window.location.hash = target;
    //     });
    // });
    // $('.header-link a').click(function(){
    //     $('html, body').animate({
    //         scrollTop: $( $(this).attr('href') ).offset().top
    //     }, 500);
    //     return false;
    // });
    $(document).on('click', 'a[href^="#"]', function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: $($.attr(this, 'href')).offset().top - 83
        }, 900);
    });


/// emd smooth scroll

// add class siderbar
// $('.mobile-icon').click(function() {
$(document).on('click', '.mobile-icon', function () {
    $('.mobile-menu').addClass('active');
});
$(document).on('click','.overlay, .mobile-link ul li a,.close-menu,.btn-lg', function () {
    $('.mobile-menu').removeClass('active');
});

// validation
$(document).ready(function() {
document.getElementById('phone').addEventListener('input', function (e) {
  var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
  e.target.value = !x[2] ? x[1] :  x[1] + '-' + x[2] + (x[3] ? '-' + x[3] : '');
});
});


// CARD VALIDATION
$(document).ready(function() {
  var acceptedCreditCards = {
    visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
    mastercard: /^5[1-5][0-9]{14}$|^2(?:2(?:2[1-9]|[3-9][0-9])|[3-6][0-9][0-9]|7(?:[01][0-9]|20))[0-9]{12}$/,
    amex: /^3[47][0-9]{13}$/,
    discover: /^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$/,
    diners_club: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
    jcb: /^(?:2131|1800|35[0-9]{3})[0-9]{11}$/,
  };
  
  $('#cc').on('input', function(){
    // if (validateCard($('#cc').val()) && validateCVV($('#cc').val())) {
    //   $('button[type="submit"]').prop('disabled', false);
    // } else {
    //   $('button[type="submit"]').prop('disabled', true);
    // }
    
    var node = $('#cc')[0]; // vanilla javascript element
    var cursor = node.selectionStart; // store cursor position
    var lastValue = $('#cc').val(); // get value before formatting
    
    var formattedValue = formatCardNumber(lastValue);
    $('#cc').val(formattedValue); // set value to formatted
    
    // keep the cursor at the end on addition of spaces
    if(cursor === lastValue.length) {
      cursor = formattedValue.length;
      // decrement cursor when backspacing
      // i.e. "4444 |" => backspace => "4444|"
      if($('#cc').attr('data-lastvalue') && $('#cc').attr('data-lastvalue').charAt(cursor - 1) == " ") {
        cursor--;
      }
    }
  
    if (lastValue != formattedValue) {
      // increment cursor when inserting character before a space
      // i.e. "1234| 6" => "5" typed => "1234 5|6"
      if(lastValue.charAt(cursor) == " " && formattedValue.charAt(cursor - 1) == " ") {
        cursor++;
      }
    }
    
    // set cursor position
    node.selectionStart = cursor;
    node.selectionEnd = cursor;
    // store last value
    $('#cc').attr('data-lastvalue', formattedValue);
  });
  
  function formatCardNumber(value) {
    // remove all non digit characters
    var value = value.replace(/\D/g, '');
    var formattedValue;
    var maxLength;
    // american express, 15 digits
    if ((/^3[47]\d{0,13}$/).test(value)) {
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{6})/, '$1 $2 ');
      maxLength = 17;
    } else if((/^3(?:0[0-5]|[68]\d)\d{0,11}$/).test(value)) { // diner's club, 14 digits
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{6})/, '$1 $2 ');
      maxLength = 16;
    } else if ((/^\d{0,16}$/).test(value)) { // regular cc number, 16 digits
      formattedValue = value.replace(/(\d{4})/, '$1 ').replace(/(\d{4}) (\d{4})/, '$1 $2 ').replace(/(\d{4}) (\d{4}) (\d{4})/, '$1 $2 $3 ');
      maxLength = 19;
    }
    
    $('#cc').attr('maxlength', maxLength);
    return formattedValue;
  }
  
  
  function validateCard(value) {
    // remove all non digit characters
    var value = value.replace(/\D/g, '');
    var sum = 0;
    var shouldDouble = false;
    // loop through values starting at the rightmost side
    for (var i = value.length - 1; i >= 0; i--) {
      var digit = parseInt(value.charAt(i));
  
      if (shouldDouble) {
        if ((digit *= 2) > 9) digit -= 9;
      }
  
      sum += digit;
      shouldDouble = !shouldDouble;
    }
    
    var valid = (sum % 10) == 0;
    var accepted = false;
    
    // loop through the keys (visa, mastercard, amex, etc.)
    Object.keys(acceptedCreditCards).forEach(function(key) {
      var regex = acceptedCreditCards[key];
      if (regex.test(value)) {
        accepted = true;
      }
    });
    
    return valid && accepted;
  }
  
  
  function validateCVV(creditCard, cvv) {
    // remove all non digit characters
    var creditCard = creditCard.replace(/\D/g, '');
    var cvv = cvv.replace(/\D/g, '');
    // american express and cvv is 4 digits
    if ((acceptedCreditCards.amex).test(creditCard)) {
      if((/^\d{4}$/).test(cvv))
        return true;
    } else if ((/^\d{3}$/).test(cvv)) { // other card & cvv is 3 digits
      return true;
    }
    return false;
  }
  
});


$(document).ready(function() {
  var expiryMask = function() {
    var inputChar = String.fromCharCode(event.keyCode);
    var code = event.keyCode;
    var allowedKeys = [8];
    if (allowedKeys.indexOf(code) !== -1) {
        return;
    }

    event.target.value = event.target.value.replace(
        /^([1-9]\/|[2-9])$/g, '0$1/'
    ).replace(
        /^(0[1-9]|1[0-2])$/g, '$1/'
    ).replace(
        /^([0-1])([3-9])$/g, '0$1/$2'
    ).replace(
        /^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2'
    ).replace(
        /^([0]+)\/|[0]+$/g, '0'
    ).replace(
        /[^\d\/]|^[\/]*$/g, ''
    ).replace(
        /\/\//g, '/'
    );
}

var splitDate = function($domobj, value) {
    var regExp = /(1[0-2]|0[1-9]|\d)\/(20\d{2}|19\d{2}|0(?!0)\d|[1-9]\d)/;
    var matches = regExp.exec(value);
    $domobj.siblings('input[name$="expiryMonth"]').val(matches[1]);
    $domobj.siblings('input[name$="expiryYear"]').val(matches[2]);
}

$('.expiry').on('keyup', function(){
    expiryMask();
});

$('.expiry').on('focusout', function(){
    splitDate($(this), $(this).val());
});
});